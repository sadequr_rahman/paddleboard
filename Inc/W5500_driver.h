#ifndef __W5500HARDWAREDRIVER_H_
#define __W5500HARDWAREDRIVER_H_

#include <stdint.h>
#include "stm32f1xx_hal.h"

#define W5500_RESET_PORT  GPIOB
#define W5500_CS_PORT			GPIOB
#define W5500_CS_PIN			W5500_CS_Pin
#define W5500_RESET_PIN		W5500_RST_Pin


void W5500Spi_Init(void);
void W5500_Reset(void);

#endif
