#ifndef PACKETPARSER_H
#define PACKETPARSER_H

#include <stdint.h>

#define Start_Byte_idx      0
#define Msg_Type_idx        1
#define Seq0_idx            2
#define Seq1_idx            3
#define Data_len0_idx       4
#define Data_len1_idx       5
#define Packet_Header_Size  6
#define Start_byte          0


typedef enum {
    ConfigureInputs = 0x43,
    InputStatusRequest = 0x52,
    SetInputs = 0x53,
    SetOutputs = 0x4f,
    InputStatus = 0x49,
    Acknowledgement = 0x4b
}message_t;


typedef struct{
    message_t msgType;
    uint8_t sequence0;
		uint8_t sequence1;
    uint16_t dataLength;
}packet_header_t;

typedef struct{
    uint8_t paddleDirMask;
    uint8_t paddleWidthMask;
    uint8_t autoWindingMask;
    uint8_t countPerStepMask;
}config_packet_t;

typedef struct{
    uint8_t hardwareState;
    uint8_t paddleDirState;
    uint16_t paddleWidthState;
    uint8_t autoWindingState;
    uint8_t countPerStepState;
}inputStatus_packet_t;

typedef struct{
    uint8_t inputSource;
    uint8_t paddleDirState;
    uint16_t paddleWidthState;
    uint8_t autoWindingState;
    uint8_t countPerStepState;
}inputSet_packet_t;

typedef struct{
    uint8_t output1 : 1;
    uint8_t output2 : 1;
    uint8_t output3 : 1;
    uint8_t output4 : 1;
    uint8_t output5 : 1;
    uint8_t output6 : 1;
    uint8_t output7 : 1;
    uint8_t output8 : 1;
}outputSet_packet_t;

typedef void (*onConfigSet)(packet_header_t *_header, config_packet_t *_packet);
typedef void (*onOutputSet)(packet_header_t *_header, outputSet_packet_t *_packet);
typedef void (*onInputSet) (packet_header_t *_header, inputSet_packet_t *_inputPacket);
typedef void (*onInputStatusRequest)(packet_header_t *_header);


void setOnConfigSetCallback(onConfigSet _callback);
void setOnOutputSet(onOutputSet _callback);
void setOnInputSet(onInputSet _callback);
void setOnInputStatusRequest(onInputStatusRequest _callback);
int parsePacket(const uint8_t *array);

#endif // PACKETPARSER_H
