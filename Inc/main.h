/**
  ******************************************************************************
  * File Name          : main.h
  * Description        : This file contains the common defines of the application
  ******************************************************************************
  ** This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * COPYRIGHT(c) 2017 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H
  /* Includes ------------------------------------------------------------------*/

/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private define ------------------------------------------------------------*/

#define InputMask_3_Pin GPIO_PIN_13
#define InputMask_3_GPIO_Port GPIOC
#define InputMask_4_Pin GPIO_PIN_0
#define InputMask_4_GPIO_Port GPIOA
#define InputMask_5_Pin GPIO_PIN_1
#define InputMask_5_GPIO_Port GPIOA
#define InputMask_6_Pin GPIO_PIN_2
#define InputMask_6_GPIO_Port GPIOA
#define InputMask_7_Pin GPIO_PIN_3
#define InputMask_7_GPIO_Port GPIOA
#define InputMask_8_Pin GPIO_PIN_4
#define InputMask_8_GPIO_Port GPIOA
#define OutputMask_4_Pin GPIO_PIN_0
#define OutputMask_4_GPIO_Port GPIOB
#define OutputMask_3_Pin GPIO_PIN_1
#define OutputMask_3_GPIO_Port GPIOB
#define OutputMask_2_Pin GPIO_PIN_10
#define OutputMask_2_GPIO_Port GPIOB
#define OutputMask_1_Pin GPIO_PIN_11
#define OutputMask_1_GPIO_Port GPIOB
#define OutputMask_5_Pin GPIO_PIN_12
#define OutputMask_5_GPIO_Port GPIOB
#define OutputMask_6_Pin GPIO_PIN_13
#define OutputMask_6_GPIO_Port GPIOB
#define OutputMask_7_Pin GPIO_PIN_14
#define OutputMask_7_GPIO_Port GPIOB
#define OutputMask_8_Pin GPIO_PIN_15
#define OutputMask_8_GPIO_Port GPIOB
#define sourceSelect_Pin GPIO_PIN_8
#define sourceSelect_GPIO_Port GPIOA
#define OutputMask_1A11_Pin GPIO_PIN_11
#define OutputMask_1A11_GPIO_Port GPIOA
#define countPerStep_Pin GPIO_PIN_12
#define countPerStep_GPIO_Port GPIOA
#define aw_Pin GPIO_PIN_15
#define aw_GPIO_Port GPIOA
#define ccw_Pin GPIO_PIN_3
#define ccw_GPIO_Port GPIOB
#define cw_Pin GPIO_PIN_4
#define cw_GPIO_Port GPIOB
#define W5500_RST_Pin GPIO_PIN_5
#define W5500_RST_GPIO_Port GPIOB
#define W5500_int_Pin GPIO_PIN_6
#define W5500_int_GPIO_Port GPIOB
#define W5500_int_EXTI_IRQn EXTI9_5_IRQn
#define W5500_CS_Pin GPIO_PIN_7
#define W5500_CS_GPIO_Port GPIOB
#define InputMask_1_Pin GPIO_PIN_8
#define InputMask_1_GPIO_Port GPIOB
#define InputMask_2_Pin GPIO_PIN_9
#define InputMask_2_GPIO_Port GPIOB

/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

void _Error_Handler(char *, int);

#define Error_Handler() _Error_Handler(__FILE__, __LINE__)

/**
  * @}
  */ 

/**
  * @}
*/ 

#endif /* __MAIN_H */
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
