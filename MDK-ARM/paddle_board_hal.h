#ifndef __PADDLE_BOARD_HAL_H__
#define __PADDLE_BOARD_HAL_H__

#include <stdint.h>

typedef enum 
{
	paddle = 0x50,
	toshiba = 0x54
}source_t;


typedef struct 
{
	source_t source;
	uint8_t direction;
	uint16_t paddleWidth;
	uint8_t autoWinding;
	uint8_t countPerStep;
}paddle_board_t;


void paddleBoardHal_Init(void);
void paddleBoardHal_setWidth(uint16_t _wid);
uint16_t paddleBoardHal_getWidth(void);
void paddleBoardHal_setUpdateMood(void);
void paddleBoardHal_resetUpdateMood(void);
void paddleBoardHal_setAutoWinding(uint8_t _val);
uint8_t paddleBoardHal_getAutoWinding(void);
void paddleBoardHal_setCountPerStep(uint8_t _val);
uint8_t paddleBoardHal_getCountPerStep(void);
void paddleBoardHal_setDirection(uint8_t _val);
uint8_t paddleBoardHal_getDirection(void);
void paddleBoardHal_setSource(source_t _src);
source_t paddleBoardHal_getSource(void);

uint8_t paddleBoardHal_getInputState(void);


#endif /* __PADDLE_BOARD_HAL_H__ */
