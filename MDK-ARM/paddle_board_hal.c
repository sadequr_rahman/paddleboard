#include "paddle_board_hal.h"
#include "stm32f1xx_hal.h"

/*
		private deination
*/
#define cw_high()						HAL_GPIO_WritePin(cw_GPIO_Port,cw_Pin,GPIO_PIN_RESET)
#define cw_low()						HAL_GPIO_WritePin(cw_GPIO_Port,cw_Pin,GPIO_PIN_SET)
#define ccw_high()					HAL_GPIO_WritePin(ccw_GPIO_Port,ccw_Pin,GPIO_PIN_RESET)
#define ccw_low()						HAL_GPIO_WritePin(ccw_GPIO_Port,ccw_Pin,GPIO_PIN_SET)
#define aw_high()						HAL_GPIO_WritePin(aw_GPIO_Port,aw_Pin,GPIO_PIN_RESET)
#define aw_low()						HAL_GPIO_WritePin(aw_GPIO_Port,aw_Pin,GPIO_PIN_SET)
#define sourceSelect_high()	HAL_GPIO_WritePin(sourceSelect_GPIO_Port,sourceSelect_Pin,GPIO_PIN_SET)
#define sourceSelect_low()  HAL_GPIO_WritePin(sourceSelect_GPIO_Port,sourceSelect_Pin,GPIO_PIN_RESET)
#define countPerStep_high() HAL_GPIO_WritePin(countPerStep_GPIO_Port,countPerStep_Pin,GPIO_PIN_RESET)
#define countPerStep_low()	HAL_GPIO_WritePin(countPerStep_GPIO_Port,countPerStep_Pin,GPIO_PIN_SET)

/*
	Global variable
*/
extern TIM_HandleTypeDef htim1;

/*
	Privte variable
*/
static paddle_board_t paddleBoard;
/*
		private function decleration
*/
static void setBit(uint8_t _val, uint8_t bitNumber);
static void resetBit(uint8_t _val, uint8_t bitNumber);
/*
	Private function Implementation
*/
void setBit(uint8_t _val, uint8_t bitNumber)
{
	_val |= (1 << bitNumber);
}

void resetBit(uint8_t _val, uint8_t bitNumber)
{
	_val &= ~(1 << bitNumber);
}
/*
		public funtion Implementation
*/

void paddleBoardHal_Init(void)
{	
	paddleBoard.autoWinding = 0;
	paddleBoard.countPerStep = 0;
	paddleBoard.direction = 0;
	paddleBoard.paddleWidth = 0;
	paddleBoard.source = toshiba;
	
	cw_low();
	ccw_low();
	paddleBoardHal_setCountPerStep(paddleBoard.countPerStep);
	paddleBoardHal_setAutoWinding(paddleBoard.autoWinding);
	paddleBoardHal_setSource(paddleBoard.source);
	
	
	HAL_TIM_OC_Start_IT(&htim1, TIM_CHANNEL_1);	
	htim1.Init.Period = paddleBoard.paddleWidth;
	if (HAL_TIM_Base_Init(&htim1) != HAL_OK)
	{
		Error_Handler();
	}
	
}


void paddleBoardHal_setWidth(uint16_t _wid)
{
	paddleBoard.paddleWidth = _wid;
	htim1.Init.Period = paddleBoard.paddleWidth;
	if (HAL_TIM_Base_Init(&htim1) != HAL_OK)
	{
		Error_Handler();
	}

}

uint16_t paddleBoardHal_getWidth(void)
{
	return paddleBoard.paddleWidth;
}

void paddleBoardHal_setUpdateMood(void)
{
	if( HAL_TIM_OC_Stop_IT(&htim1,TIM_CHANNEL_1) != HAL_OK)
	{
		Error_Handler();
	}
}

void paddleBoardHal_resetUpdateMood(void)
{
	if( HAL_TIM_OC_Start_IT(&htim1, TIM_CHANNEL_1) != HAL_OK)
	{
		Error_Handler();
	}
}

uint8_t paddleBoardHal_getAutoWinding(void)
{
	return paddleBoard.autoWinding;
}

void paddleBoardHal_setAutoWinding(uint8_t _val)
{
	paddleBoard.autoWinding = _val;
	_val ? aw_high() : aw_low();
}

uint8_t paddleBoardHal_getCountPerStep(void)
{
	return paddleBoard.countPerStep;
}

void paddleBoardHal_setCountPerStep(uint8_t _val)
{
	paddleBoard.countPerStep = _val;
	_val ? countPerStep_high():countPerStep_low();
}

uint8_t paddleBoardHal_getDirection(void)
{
	return paddleBoard.direction;
}

void paddleBoardHal_setDirection(uint8_t _val)
{
	paddleBoard.direction = _val;
  paddleBoard.direction == 0 ? ccw_low() : cw_low();
}

source_t paddleBoardHal_getSource(void)
{
	return paddleBoard.source;
}

void paddleBoardHal_setSource(source_t _src)
{
	paddleBoard.direction = _src;
	switch(paddleBoard.direction)
	{
		case toshiba:
			sourceSelect_high();
			break;
		case paddle:
			sourceSelect_low();
			break;
		default:
			break;
	}
}

uint8_t paddleBoardHal_getInputState(void)
{
	uint8_t inputStatus = 0x00;
	
	if(HAL_GPIO_ReadPin(InputMask_8_GPIO_Port,InputMask_8_Pin) == 0)
		setBit(inputStatus,7);
	else
		resetBit(inputStatus,7);
	
	if(HAL_GPIO_ReadPin(InputMask_7_GPIO_Port,InputMask_7_Pin) == 0)
		setBit(inputStatus,6);
	else
		resetBit(inputStatus,6);
	
	if(HAL_GPIO_ReadPin(InputMask_6_GPIO_Port,InputMask_6_Pin) == 0)
		setBit(inputStatus,5);
	else
		resetBit(inputStatus,5);
	
	if(HAL_GPIO_ReadPin(InputMask_5_GPIO_Port,InputMask_5_Pin) == 0)
		setBit(inputStatus,4);
	else
		resetBit(inputStatus,4);
	
	if(HAL_GPIO_ReadPin(InputMask_4_GPIO_Port,InputMask_4_Pin) == 0)
		setBit(inputStatus,3);
	else
		resetBit(inputStatus,3);
	
	if(HAL_GPIO_ReadPin(InputMask_3_GPIO_Port,InputMask_3_Pin) == 0)
		setBit(inputStatus,2);
	else
		resetBit(inputStatus,2);
	
	if(HAL_GPIO_ReadPin(InputMask_2_GPIO_Port,InputMask_2_Pin) == 0)
		setBit(inputStatus,1);
	else
		resetBit(inputStatus,1);
	
	if(HAL_GPIO_ReadPin(InputMask_1_GPIO_Port,InputMask_1_Pin) == 0)
		setBit(inputStatus,0);
	else
		resetBit(inputStatus,0);
	
	
	return inputStatus;
	
}


/**
* @brief This function handles TIM1 capture compare interrupt.
*/
void TIM1_CC_IRQHandler(void)
{
  HAL_TIM_IRQHandler(&htim1);
	if( paddleBoard.direction == 0 ) 
	{
		HAL_GPIO_TogglePin(cw_GPIO_Port,cw_Pin);
	}
	else 
	{
		HAL_GPIO_TogglePin(ccw_GPIO_Port,ccw_Pin);
	}
}

