#include <wizchip_conf.h>
#include <w5500.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <socket.h>
#include "paddle_board.h"
#include "W5500_driver.h"
#include "packetparser.h"
#include "paddle_board_hal.h"



#define SEPARATOR            	"=============================================\r\n"
#define WELCOME_MSG  		 			"Welcome to STM32Nucleo Ethernet configuration\r\n"
#define NETWORK_MSG  		 			"Network configuration:\r\n"
#define IP_MSG 		 		 				"  IP ADDRESS:  %d.%d.%d.%d\r\n"
#define NETMASK_MSG	         	"  NETMASK:     %d.%d.%d.%d\r\n"
#define GW_MSG 		 		 				"  GATEWAY:     %d.%d.%d.%d\r\n"
#define MAC_MSG		 		 				"  MAC ADDRESS: %x:%x:%x:%x:%x:%x\r\n"
#define GREETING_MSG 		 			"Well done guys! Welcome to the IoT world. Bye!"
#define CONN_ESTABLISHED_MSG 	"Connection established with remote IP: %d.%d.%d.%d:%d\r\n"
#define SENT_MESSAGE_MSG	 		"Sent a message. Let's close the socket!\r\n"
#define WRONG_RETVAL_MSG	 		"Something went wrong; return value: %d\r\n"
#define WRONG_STATUS_MSG	 		"Something went wrong; STATUS: %d\r\n"
#define LISTEN_ERR_MSG		 		"LISTEN Error!\r\n"
#define Zoneinfo		 		 			"  zone info: %x:%x:%x:%x:%x:%x\r\n"


#define PRINT_STR(msg) do  {										\
  printf(msg);																	\
} while(0)

#define PRINT_HEADER() do  {													\
  printf(SEPARATOR);																	\
  printf(WELCOME_MSG);																\
  printf(SEPARATOR);																	\
} while(0)

#define PRINT_NETINFO(netInfo) do { 																					\
  printf(MAC_MSG, netInfo.mac[0], netInfo.mac[1], netInfo.mac[2], netInfo.mac[3], netInfo.mac[4], netInfo.mac[5]);\
  printf(IP_MSG, netInfo.ip[0], netInfo.ip[1], netInfo.ip[2], netInfo.ip[3]);										\
  printf(NETMASK_MSG, netInfo.sn[0], netInfo.sn[1], netInfo.sn[2], netInfo.sn[3]);								\
  printf(GW_MSG, netInfo.gw[0], netInfo.gw[1], netInfo.gw[2], netInfo.gw[3]);										\
} while(0)

/* 
		GlobalVariable 
*/
wiz_NetInfo gWIZNETINFO = 
{
  .mac = {ETHADDR0, ETHADDR1, ETHADDR2, ETHADDR3, ETHADDR4, ETHADDR5},
  .ip = {IPADDR0, IPADDR1, IPADDR2, IPADDR3},
  .sn = {NETMASK0, NETMASK1, NETMASK2, NETMASK3},
  .gw = {DRIPADDR0, DRIPADDR1, DRIPADDR2, DRIPADDR3},
  .dns = {DRIPADDR0, DRIPADDR1, DRIPADDR2, DRIPADDR3},
  .dhcp = NETINFO_DHCP //NETINFO_STATIC
};


/* 
			Private variables 
*/
static uint8_t socketInterruptFlag = 0;
//static uint8_t isOpen = 0;
static uint8_t socketId = 0;
static socket_state_t currentState = socket_openning;
static uint16_t localPort = 8210;
static uint16_t destPort = 8200;
static uint8_t destIp[4] = {192,168,1,99};
//static char debugMsg[100];
/* 
			private function declaration 
*/
static void acknowledgeInterrupt(void);
static void onConfigSetListener(packet_header_t *_header, config_packet_t* _packet);
static void onOutputSetListener(packet_header_t *_header, outputSet_packet_t* _packet);
static void onInputSetListener(packet_header_t *_header, inputSet_packet_t* _packet);
static void onInputStatusRequestListener(packet_header_t *_header);
static void sendAck(packet_header_t *_head);
/*
			private function implementation 
*/
void acknowledgeInterrupt(void)
{
	 setIR(0x10);
	 setSIR(0x01);
	 setSn_IR(0,0xff);	
}

void sendAck(packet_header_t *_head)
{
	uint8_t fbStream[Packet_Header_Size];
	_head->msgType = Acknowledgement;
	memcpy((void*)fbStream,(void*)_head,Packet_Header_Size);
	sendto(socketId,fbStream,Packet_Header_Size,destIp,destPort);
}

void onConfigSetListener(packet_header_t *_header, config_packet_t *_packet)
{
	printf("autoWindingMask: %d\ncountPerStepMask: %d\npaddleDirMask: %d\npaddleWidthMask: %d\n",
											_packet->autoWindingMask,_packet->countPerStepMask,
											_packet->paddleDirMask,_packet->paddleWidthMask);
	
	
	sendAck(_header);
}

void onOutputSetListener(packet_header_t *_header, outputSet_packet_t* _packet)
{
	printf("output1: %d\n\routput2: %d\n\routput3: %d\n\routput4: %d\n\routput5: %d\n\routput6: %d\n\routput7: %d\n\routput8: %d\n\r",
							_packet->output1,_packet->output2,_packet->output3,_packet->output4,_packet->output5,_packet->output6,_packet->output7,_packet->output8);
	
	sendAck(_header);
}

void onInputSetListener(packet_header_t *_header, inputSet_packet_t* _packet)
{
	printf("autoWindingState: %d\n\rcountPerStepState: %d\n\rinputSource: %d\n\rpaddleDirState: %d\n\rpaddleWidthState: %d\n\r",
											_packet->autoWindingState,_packet->countPerStepState,
											_packet->inputSource,_packet->paddleDirState,_packet->paddleWidthState);
	
	paddleBoardHal_setUpdateMood();
	switch(_packet->inputSource)
	{
		case paddle:
			paddleBoardHal_setSource(paddle);
			paddleBoardHal_setAutoWinding(_packet->autoWindingState);
			paddleBoardHal_setCountPerStep(_packet->countPerStepState);
			paddleBoardHal_setDirection(_packet->paddleDirState);
			paddleBoardHal_setWidth(_packet->paddleWidthState);
			break;
		case toshiba:
			paddleBoardHal_setSource(toshiba);
			break;
		default:
			break;
	}
	
	sendAck(_header);
	paddleBoardHal_resetUpdateMood();
}


void onInputStatusRequestListener(packet_header_t *_header)
{
	inputStatus_packet_t sample;
	sample.autoWindingState = paddleBoardHal_getAutoWinding();
	sample.countPerStepState = paddleBoardHal_getCountPerStep();
	sample.hardwareState = paddleBoardHal_getInputState();
	sample.paddleDirState = paddleBoardHal_getDirection();
	sample.paddleWidthState = paddleBoardHal_getWidth();
	_header->msgType = InputStatus;
	_header->dataLength = sizeof(sample);
	uint8_t *packet = (uint8_t*) malloc(Packet_Header_Size+sizeof(sample));
	memcpy((void*)&packet[0],(const void*)_header,Packet_Header_Size);
	memcpy((void*)&packet[Packet_Header_Size],(const void*)&sample,sizeof(sample));
	printf("Sending FeedBack\r\n");
	sendto(socketId,packet,Packet_Header_Size+sizeof(sample),destIp,destPort);
	free(packet);
	
}



/* 
			public function implementation 
*/

void paddleBoard_Init(void)
{
	printf("Paddle Board Init Function\r\n");
	printf("Waitting for phy Link\r\n");
	W5500Spi_Init();
	PRINT_HEADER();
	CONFIG:
	wizchip_setnetinfo(&gWIZNETINFO);
  setSHAR(gWIZNETINFO.mac);
	
	
	wiz_NetInfo netInfo;
	wizchip_getnetinfo(&netInfo);
	PRINT_NETINFO(netInfo);
	
	if(netInfo.ip[0] == gWIZNETINFO.ip[0] && netInfo.ip[1] == gWIZNETINFO.ip[1] && netInfo.ip[2] == gWIZNETINFO.ip[2] && netInfo.ip[3] == gWIZNETINFO.ip[3])
	{
		printf("Config Okay\r\n");
		HAL_GPIO_WritePin(OutputMask_6_GPIO_Port,OutputMask_6_Pin,GPIO_PIN_SET);
	}
	else
	{
		HAL_GPIO_WritePin(OutputMask_6_GPIO_Port,OutputMask_6_Pin,GPIO_PIN_RESET);
		goto CONFIG;
	}
	
	//PRINT_NETINFO(gWIZNETINFO);
	uint16_t it = wizchip_getinterruptmask();
//	sprintf(debugMsg,"Before Interrupt Mask: %0X \r\n",it);
//	printf("%s",debugMsg);
	wizchip_setinterruptmask(IK_SOCK_0);
	
	it = wizchip_getinterruptmask();
//	sprintf(debugMsg,"After Interrupt Mask: %0X \r\n",it);
//	printf("%s",debugMsg);
	
	setOnConfigSetCallback(onConfigSetListener);
	setOnInputSet(onInputSetListener);
	setOnInputStatusRequest(onInputStatusRequestListener);
	setOnOutputSet(onOutputSetListener);
	
	paddleBoardHal_Init();
}


void paddleBoard_AsyncTask(void)
{
		HAL_GPIO_TogglePin(OutputMask_7_GPIO_Port,OutputMask_7_Pin);
		if(socketInterruptFlag == 1)
		{
			uint8_t associateSocket = getSIR();
			//printf("Interrupt caused by socket id: %d\r\n",associateSocket);
			if(associateSocket == 0x01)
			{
				currentState = socket_receive;
			}
			
			socketInterruptFlag = 0;
			acknowledgeInterrupt();
		}
		
		switch(currentState)
		{
			case socket_openning:
				printf("Openning Socket..\r\n");
				if(socket(socketId,Sn_MR_UDP,localPort,0) == socketId)
				{
					printf("Socket Open Successful\r\n");
					currentState = socket_idle;
				  HAL_GPIO_WritePin(OutputMask_1_GPIO_Port,OutputMask_1_Pin,GPIO_PIN_SET);
					//isOpen = 1;
				}
				else
				{
					printf("Socket Open Failed. Retry After 1s\r\n");
					HAL_GPIO_WritePin(OutputMask_1_GPIO_Port,OutputMask_1_Pin,GPIO_PIN_RESET);
				}
			break;
			case socket_receive:
					printf("Socket Data Recevied\r\n");
					{
						uint16_t recSize = getSn_RX_RSR(0);
						//printf(debugMsg,"RecSize: %d\n",recSize);
						if(recSize > 0)
						{
							uint8_t *packet = NULL;
							do{
								 packet = (uint8_t*) malloc(recSize);
							}while(packet== NULL);
							memset(packet,0,recSize);
							wiz_recv_data(0,packet,recSize);
							uint8_t *ptr = &packet[8];
							//printf("Payload: %s\r\n",ptr);
							if(parsePacket(ptr) == 0)
							{
								printf("Parse Okay\n\r");
							}
							else
							{
								printf("Parse Error\n\r");
							}
							
							free (packet);	
							setSn_CR(0,Sn_CR_RECV);
							while(getSn_CR(0));
						}
								
					}
					currentState = socket_idle;
			break;
			case socket_idle:
					if(!(getPHYCFGR() & 0x01))
					{
						printf("Phy Link Down\r\n");
						HAL_GPIO_WritePin(OutputMask_2_GPIO_Port,OutputMask_2_Pin,GPIO_PIN_RESET);
					}
					else
					{
						HAL_GPIO_WritePin(OutputMask_2_GPIO_Port,OutputMask_2_Pin,GPIO_PIN_SET);
					}
					if(getSn_SR(socketId) != SOCK_UDP)
					{
						printf("Socket error\r\n");
						
					}
			break;
			default:
			break;
		}

}


/**
* @brief This function handles EXTI line[9:5] interrupts.
*/
void EXTI9_5_IRQHandler(void)
{
  HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_6);
	socketInterruptFlag = 1;
}

