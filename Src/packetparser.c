#include "packetparser.h"
#include <string.h>

static onConfigSet configCallback = NULL;
static onInputSet  inputSetCallback = NULL;
static onOutputSet outputSetCallback = NULL;
static onInputStatusRequest inputStatusCallback = NULL;
//static uint8_t hexDigitToUint(char digit);

//uint8_t hexDigitToUint(char digit)
//{
//    uint8_t v = (digit > '9')? (digit &~ 0x20) - 'A' + 10: (digit - '0');
//    return v;
//}

int parsePacket(const uint8_t *array)
{
    int status = -1;
    if(array[Start_Byte_idx] == Start_byte)
    {
        packet_header_t _packetHeader;
        _packetHeader.msgType = (message_t)array[Msg_Type_idx];
        //_packetHeader.sequence = (hexDigitToUint(array[Seq1_idx]) + 16 )+ (hexDigitToUint(array[Seq0_idx]));
				_packetHeader.sequence0 = array[Seq0_idx];
				_packetHeader.sequence1 = array[Seq1_idx];
        _packetHeader.dataLength = ((uint16_t)array[Data_len1_idx] << 8) + array[Data_len0_idx];

        switch (_packetHeader.msgType) {
        case ConfigureInputs:
        {
                config_packet_t configPacket;
                configPacket.paddleDirMask = array[Packet_Header_Size+0];
                configPacket.paddleWidthMask = array[Packet_Header_Size+1];
                configPacket.autoWindingMask = array[Packet_Header_Size+2];
                configPacket.countPerStepMask = array[Packet_Header_Size+3];
                if(configCallback != NULL)
                {
                    (*configCallback)(&_packetHeader,&configPacket);
                }
                status = 0;
        }
            break;
        case InputStatusRequest:
				{
                if(inputStatusCallback != NULL)
                {
										
                    (*inputStatusCallback)(&_packetHeader);
                }
				}
                status = 0;
            break;
        case SetInputs:
        {
             inputSet_packet_t inputPacket;
             inputPacket.inputSource = array[Packet_Header_Size+0];
             inputPacket.paddleDirState = array[Packet_Header_Size+1];
             inputPacket.paddleWidthState = ( (uint16_t) array[Packet_Header_Size+3] << 8) + array[Packet_Header_Size+2];
             inputPacket.autoWindingState = array[Packet_Header_Size+4];
             inputPacket.countPerStepState = array[Packet_Header_Size+5];
             if(inputSetCallback != NULL)
             {
                 (*inputSetCallback)(&_packetHeader,&inputPacket);
             }
             status = 0;
        }
            break;
        case SetOutputs:
        {
            outputSet_packet_t outputSetPacket;
            memcpy((void*)&outputSetPacket,(void*)&array[Packet_Header_Size+0],1);
            if(outputSetCallback != NULL)
            {
                (*outputSetCallback)(&_packetHeader,&outputSetPacket);
            }
            status = 0;
        }
            break;
        case Acknowledgement:
                
						status = 0;
            break;
        default:
            break;
        }


    }

    return status;
}

void setOnConfigSetCallback(onConfigSet _callback)
{
    if(configCallback == NULL)
         configCallback = _callback;
}

void setOnOutputSet(onOutputSet _callback)
{
    if(outputSetCallback==NULL)
         outputSetCallback = _callback;
}

void setOnInputSet(onInputSet _callback)
{
    if(inputSetCallback == NULL)
        inputSetCallback = _callback;
}

void setOnInputStatusRequest(onInputStatusRequest _callback)
{
    if(inputStatusCallback == NULL)
        inputStatusCallback = _callback;
}
