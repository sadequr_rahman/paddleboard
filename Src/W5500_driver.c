#include "W5500_driver.h"
#include "wizchip_conf.h"
#include <stdio.h>

extern SPI_HandleTypeDef hspi1;


static void W5500Select() 
{
	HAL_GPIO_WritePin(W5500_CS_PORT, W5500_CS_PIN, GPIO_PIN_RESET); //CS LOW
}

static void W5500DeSelect() 
{
	HAL_GPIO_WritePin(W5500_CS_PORT,W5500_CS_PIN,GPIO_PIN_SET); //CS HIGH
}

static uint8_t W5500ReadByte(void) 
{
	uint8_t reardBufffer;
	HAL_SPI_Receive(&hspi1, &reardBufffer, 1, 0xFFFFFFFF);
	return reardBufffer;
}

static void W5500WriteByte(uint8_t b) 
{
	HAL_SPI_Transmit(&hspi1, &b, 1, 0xFFFFFFFF);
}

void W5500_chipInit(void)
{
  uint8_t temp;
  uint8_t W5500FifoSize[2][8] = {{2, 2, 2, 2, 2, 2, 2, 2, }, {2, 2, 2, 2, 2, 2, 2, 2}};

  /* spi function register */
  reg_wizchip_spi_cbfunc(W5500ReadByte, W5500WriteByte);

  /* CS function register */
  reg_wizchip_cs_cbfunc(W5500Select, W5500DeSelect);

  if (ctlwizchip(CW_INIT_WIZCHIP, (void*)W5500FifoSize) == -1)
    printf("W5500 initialized fail.\r\n");
  
  //check phy status
  do
  {
    if (ctlwizchip(CW_GET_PHYLINK, (void*)&temp) == -1)
    {
      printf("Unknown PHY link status.\r\n");
    }
  } while (temp == PHY_LINK_OFF);
}


void W5500_Reset(void)
{
  HAL_GPIO_WritePin(W5500_RESET_PORT, W5500_RESET_PIN,GPIO_PIN_RESET);
  HAL_Delay(500);
  HAL_GPIO_WritePin(W5500_RESET_PORT, W5500_RESET_PIN,GPIO_PIN_SET);
  HAL_Delay(10);
}

void W5500Spi_Init(void)
{
	W5500_Reset();
	W5500_chipInit();
}

